在潛入洞窟前艾夏換完了衣服，終於準備就緒了。

穿著黑白的膝上女僕服的艾夏無比可愛。集中了即使是在從遊戲時代就存在的拘泥於外觀的裝備，也就是所謂的魅惑裝備之中也是實用的高性能的東西交給了艾夏。當然普通的裝備裡只看性能的話好東西很多但是這裡絶不退讓。沒有理由放棄金髮少女的女僕服姿。

雖然明顯不是戰鬥的打扮，儘管如此也是全身特質級（Unique）的裝備。如果是盜賊程度的攻擊的話，將物理、魔法一起彈回程度的防御力還是有的吧。
嘛，說到底，只要有娜哈特在，就絶對不會容許對艾夏動手。

「來，走吧，艾夏──也許稍微會有點怪誕的影像，沒事吧？」
「希望您不要以郊游的心情說，娜哈特大人⋯⋯而且都事已至此了啦──」

艾夏第一次遇到娜哈特時也該說是多少嗎，被迫看到了相當厲害的影像。娜哈特的警告真的是事已至此為時已晚了。
對艾夏來說死亡常在身旁。

誰都知道人生就是稍微一個契機就會至死。
艾夏自身也不例外，艾夏的父親也不例外。
所以，娜哈特的擔心是杞人憂天。

「是嗎──話說，還真深吶」

大概是自然形成的洞窟越探視深處越微暗得令人害怕。
入口旁存在著大概是把圓木加工了的街壘，強行衝鋒的話很容易能想像到會有箭雨飛來。實際靠娜哈特的知覺就感覺到有好幾個人正眼巴巴地等待著獵物進來。

本來娜哈特是不適合在這種狹窄、逃匿處很少的地方的戰鬥的。
要說為什麼的話，因為娜哈特的攻擊手段大多是看起來很漂亮的廣範圍殲滅系魔法，在接近戰很弱，因此這種狹窄的地方會變成單方面不利的地形。再加上，為了不破壊洞窟本身，使用的魔法也被限制了。

「好麻煩啊⋯⋯一起吹飛了不行嗎⋯⋯？」
「不行啦！一定也有人質的人之類的。不救她們嗎？」

對艾夏不安的聲音，娜哈特讓集中在手上的魔力霧散了。
如果沒有被囚禁在裡面的人的話，娜哈特肯定會選擇連同洞窟把盜賊消滅的手段吧。

「艾夏，要好好貼著我哦」

麻煩的是，在這裡的盜賊們合作相當好。
不光是前方的部隊，從那前面分成了三路，企圖把前方的敵兵作為誘餌然後從左右夾擊也看得出他們是想要利用地形巧妙地協作戰鬥吧。

「是，是的！」

那麼為何娜哈特要向洞窟衝鋒呢。
理由有兩個。
一是不覺得盜賊的攻擊能穿透娜哈特的防具。
另一個是因為在黑暗的洞窟裡能合法的和艾夏貼著，僅此而已。

一步兩步的走起來，踏出第三步的那一瞬間。箭雨向娜哈特她們猛撲過來。
數量是十支，那表示了盜賊的人數，立刻就會有第二支箭迫近吧。
娜哈特警戒的首先是箭的種類。如果窮極弓手（Archer）的職業的話，無限生出威力巨大的魔矢連續發射什麼的絶技也是可能的。
但是，眼前迫近的箭怎麼看都是普通的木箭。
和撕裂空氣的尖銳聲音一起迫近的無處可逃的箭之壁。
在看起來像是窮途末路的狀況之中，聽得見娜哈特的嘆息的人只有緊緊貼著的艾夏吧。

「真無聊吶──」

說話的同時娜哈特的魔力一瞬間顯示了光輝。

「呀！」

暴風突然狂吹。
艾夏的女僕服盛大地飄起，她因此染上了羞恥。艾夏好像也有盯著娜哈特程度的餘裕。
暴風變成力之塊並收束，宛如重力場般壓碎了箭矢。

不僅如此。
受到娜哈特制御的風取得了像有意識的鐮鼬在玩耍著一樣的形態。一次眨眼的工夫三個頭飛到空中，兩次眨眼時六個頭飛到了空中。

「為，什，麼⋯⋯⋯⋯？」

沒有理解在自己身上發生了什麼，盜賊們的意識就向黑暗中啟程了。

根據陷入幻術的盜賊們的話，據說八十三人的盜賊中七十人是普通的農民。然後，有戰鬥經驗的傭兵的同伴只有十三人，能接住娜哈特的魔法的人一個也沒有。
不，最多也就一個吧。
小小的笑容，那也是些許的期待。

還有另一個。娜哈特有件好奇的事。那就是關於在這個世界的攻擊手段，被稱為武術（Arts）的奇怪的技藝（技能）。好像那是不僅限於人，所有生物積累修練到達的技藝的極意，不過那些和Real World Online的職業技能（Job Skill）有點不同。
雖然不能籠統地說是劣化，但是不由得感覺威力很低。

「去死吧，怪物──武術（Arts）──千矢的心得！！」

例如，現在被放出的一支箭不知不覺變成幾十支，一邊加速一邊猛撲過來的武術（Arts）。這就像是把弓手系統的初級技能影矢分身和流弓加起來除二一樣。
儘管像是繼承了複數的技能（Skill），但威力並不那麼高。

「盛大地起舞吧，鐮鼬──」

對飛來的全部箭矢，鮮明得肉眼可見的風盤繞後消去了箭矢。折斷、變形的箭頭徒勞地扎入地面。

「什，可惡，千矢的──」

盜賊想要再次放出攻擊。
不過，第一次只是為了看而故意讓他放的，娜哈特沒打算許可第二次。
那正和雷鳴一樣，和認識到發光了時雷已經落下了同樣，以為娜哈特的身影消失了時已經──

「嗎，呋⋯⋯⋯⋯」

──是命被收割之後了。

娜哈特的敏捷非同尋常，儘管為了不給周邊帶來災害而調節了可那個速度依然無法映在人眼中。

「接下來──還有兩人，嗎」

娜哈特侵入洞窟後大約三分鐘。
敵對勢力就已經只剩兩人了。


◇

敵人也減少了，以悠閑的步調在洞窟內走著時，貼著的艾夏的身體嚇得一抖。

強烈的鬥志飄來。
娜哈特完全不在意那個，而是看了艾夏。
娜哈特的表情簡直就像女神在微笑一樣溫柔，總覺得看起來很高興。
實際上娜哈特是很高興。
艾夏的那個敏銳知覺──純粹覺得很棒。
無論怎麼說被投來的不是害意或殺意，而只是鬥志。
儘管如此，停下了腳步就表示她的知覺甚至危險感知能力很強。

「艾夏好厲害吶──將來一定能變得很強哦」

娜哈特輕輕地撫摸著艾夏的頭時，足跡嗒，嗒的迫近過來了。

「被那樣露骨地無視的話，我可是會傷心的啊」

杜蘭沒有隱藏動靜，只是帶著仿彿要溢出了似的鬥志，來吧，戰鬥吧，邊那樣說邊前進著。
但是，娜哈特的回答很冷淡。就是說，不當一回事吧。
因為她沒在意杜蘭，而是把意識的大半朝向了艾夏。

那對杜蘭來說也是屈辱。
儘管作為鬥氣放出了擁有的全部力量，還是被無視了。
那意味著眼前的少女沒有把杜蘭作為敵人認識。
這樣的經驗還是第一次。

「沒想到──竟然是女人吶──」

克麗絲塔也是擁有龐大魔力的女戰士。但是，這是什麼。擁有常人幾倍魔力的克麗絲塔簡直沒法比。到了那種地步的異次元的存在感。

在杜蘭的心裡從未有過的煩人地鳴響了警鐘。
不可以和那個戰鬥。
沒錯，感覺本能正告知著。

肉體能力的差距用魔法輕易就會翻轉。
給了魔法使時間的話，即使是優秀的戰士也會與即死相連。

「性別是小問題──而且你這傢伙是──」
「杜蘭」

儘管插入了對話，但杜蘭拔出了大劍警戒著不讓她咏唱。

「──杜蘭什麼的為什麼在這種地方？你這傢伙的靈魂雖然粗暴，但絶不渾濁哦？硬要說的話，該說是空虛吧」

娜哈特像挑釁一樣地說道。

「──！為什麼⋯⋯不，現在那沒關係。我的加護告訴我了──下一個獵物是你吶」

杜蘭感到了只是對話就被窺視了內心深處似的錯覺。妖艷地笑著的人偶般的美少女來路不明也該有個限度。纏著金色圓環的眼瞳好像看穿了杜蘭一般。

「所以，是在模仿盜賊嗎？」

對娜哈特像譴責一樣的聲色，杜蘭敷衍地笑了。

「怎麼可能──我是打算和你戰鬥後由我搗毀的，不過省工夫了。我把首領綁了放在裡面了。如果我輸了的話那傢伙的頭也好，被捉住的人們是解放也好弄到手裡也好，隨你喜歡就好」

杜蘭從最初就打算搗毀這裡。
不可能眼睜睜地放任製造出討厭的幻影的原因不管。

「你不惜如此都想和我玩嗎？」

娜哈特故意沒說戰鬥。
但是，那個詞是否定了杜蘭的生的詞。
等同於在戰場發跡，只能在戰場上生存的杜蘭的存在被否定了。

「我想戰鬥」

對杜蘭來說那只是直覺。
但是，本能確實告知了。眼前的少女遠比杜蘭上級。
所以，那已經接近於懇求了。

「如果我戰鬥的話──你會死，而且是一瞬間，啊」

瞬間，以娜哈特為中心空氣爆發了。
將多得要溢出的大氣推開了的僅僅只是威壓。
娜哈特發動了關閉著的竜之威壓，僅此而已。

但是，對杜蘭來說那是無法相信的威壓感（壓力）

雖然想到了是上級，但那個感覺的差距簡直就像人與竜一樣。
四大竜就存在於眼前，那樣錯覺了的杜蘭儘管流下了冷汗，但還是死盯著娜哈特的眼睛。
對簡直像考官一樣高高在上地藐視的娜哈特，杜蘭快要跪下了。
但是，儘管如此還是堅守了最後一線，他開口道。

「即便是那樣，啊」

和娜哈特的預想相反，杜蘭的鬥志沒有消失。
竜像人踩爛螞蟻一樣踩爛人的，那樣的壓倒性的壓力。
但是，在這裡夾著尾巴逃走的事杜蘭做不到。
因為不那樣的話，杜蘭的生就沒有意義了。
因為戰鬥、置身於戰場才正是用這個身體取回那一天被雙親拋棄了的價值。

「──吼」

一瞬間，僅僅一瞬間娜哈特的臉上露出了笑容。

「想要戰鬥嗎？」

杜蘭是戰鬥的天才。靠廣闊的視野看清戰場，採取最有利的措施取得勝利。作為個人的武勇出色，帶領軍隊時擁有盡可能向自己的人傳送準確的指示的才能。

現在，這一瞬間，獲得勝利的最高可能性。
那就是瞄準像封住娜哈特的行動一樣緊緊摟住娜哈特的少女。那樣做的話，娜哈特一定會保護少女吧。那是杜蘭確實的預測。

可是，那樣好嗎。
對方再怎麼說是上級，將刀刃朝向沒站在戰場上的人會被容許嗎。
在杜蘭心中產生了些許的糾葛。
猶豫是一瞬間。

「覺悟──」

甩開那個時，杜蘭的身體比思考先動了。
杜蘭向娜哈特迫近的一瞬之間。
進行了身體強化的杜蘭能看出娜哈特的嘴微微張開了。
一邊承受著以全力迫近的杜蘭的氣勢，娜哈特一邊這樣說道。

──瞄準也可以哦──

被看穿了。
並沒有將視線轉向少女。並不是真的有打算瞄準。只是打算假裝那樣引起她疏忽大意，提高勝率。
但是，卻成了都被看穿了一切被挑釁的樣子。

杜蘭想要用讓盜賊的武術（Arts）瞬步進化了的瞬動高速迫近娜哈特────但沒能做到。
就像是金縛一樣，不論多麼用力身體都完全不聽使喚。
這是什麼。
那樣想著看了身體時，細細分開的淺影像要把身體捆住一樣纏繞著。

「影魔法（Shadow Magic）──居於暗影的倒戈（Shadow Bind）。你該不會以為魔法使會允許你接近吧？真是膚淺啊」

在魔法使系職業中，距離比什麼都重要。
從得意的距離單方面發起攻擊，魔法職會有利，不過這是所有職業共通的概念。

「什，庫，那⋯⋯！」

對呻吟的杜蘭，娜哈特將四個魔法球浮起。
各個染了不同的顏色。
地，水，火，風。
那毫不留情地被扔向了被拘束的杜蘭。

「庫，魔法技（Magic Arts），魔障壁──」

吸收了杜蘭的魔力大劍緩緩地閃爍。
魔法與武術的組合技是製造出對魔法使用的高抵抗力障壁的杜蘭的原創技。杜蘭的魔障壁連冰帝的魔法都會彈飛。濃厚而激烈的魔力之壁追著劍具現化了。
但是，

「嘎啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

侵蝕全身的劇烈疼痛猛撲過來。
皮膚在燒的感觸，像要毀壊身體一樣的衝擊，像要被撕裂全身一樣的斬擊，還有仿彿是附贈似的冰結將手指凍住了。

「呋姆，嘛不過如此吧──」

挖開地面後，被火烤的冰變成蒸氣充滿了視野。杜蘭遍體鱗傷是無疑的。
娜哈特正要解除臨戰態勢，但────

「還，不夠⋯⋯我還，沒有，戰鬥⋯⋯！」

儘管被自身的影子拘束，從全身流出血，杜蘭仍然沒有屈膝。
還什麼都沒做。
還什麼都沒做到。
還什麼都沒得到。
只有乾枯的渴望持續支撐著杜蘭的身體。
遍體鱗傷的杜蘭還有他那凶狠的表情一瞬間映入了如水晶般的娜哈特的瞳孔。
露出的笑容比剛才略微加深。

「──哈哈哈，是嗎────好吧，那麼你全力以赴就好」

娜哈特啪的將兩手合起一拍。
同時杜蘭的拘束解除了。

「我名叫娜哈特。我就以娜哈特・夏坦之名，接受你的意願吧」

娜哈特沒有擺出架勢，而是把艾夏放在背上後看向杜蘭。
娜哈特是姿勢是以前不曾有過的。
以前，希望PVP，認真地、拚命地、悶熱地，有時豁出性命，和那時完全不同。
但是，心情稍微有點相同。
不可思議的高揚感在胸中。
是要求真刀真槍的杜蘭的心意使然的吧。娜哈特有趣似的看了杜蘭。

「啊啊，感謝」

杜蘭採取彎腰的架勢，握著大劍。
在杜蘭的生涯中最強大的敵人如今，就在眼前。
那樣的話，放出的攻擊必須是在生涯中最強的一擊吧。
將舉過頭頂的劍進一步向上。被瞬動和身體強化支持的身體邊留下幻影邊動了。身體裡有種細胞逐個一體化了的錯覺。不知不覺地劍變成了自己的一部分，望眼欲穿地等待著決戰之時。
剎那的靜寂──

像連續敲鐘一樣鳴響的心臟如清流般安靜，噗通的跳了一下的瞬間。杜蘭隨本能命令動了起來。
將割山削地的一擊現在──

交錯是一瞬。
娜哈特的身影沒有映在杜蘭的眼中。
只是在要揮下劍的瞬間娜哈特的身影消失了。

──鏘。

那樣的聲音停止了時間。
繼承的刃發出悲鳴，徒勞地扎入地面。
殘留在杜蘭手上的感覺只有像是砍了金屬的山脈一樣的麻木。
杜蘭的意識還存在著無非是娜哈特的恩情。
如果娜哈特有心的話，頭會就那樣掉了吧。

「種族技能（Origin Skill）龍爪──龍技，神龍一閃──」
「哈哈⋯⋯⋯⋯」

被感嘆和讚賞推著漏出了笑聲。

「──確實戰鬥了哦？這樣滿足了嗎？」

雖說是物理但娜哈特對杜蘭使用了沒必要用的最上位技能。
那只是賞識他的氣魄的一時興起。
也可以說是越過了竜之威壓的褒獎。

「啊啊⋯⋯謝謝⋯⋯是我輸了──」

看得見的是一瞬的光影。
只是像幻影一樣的身姿。但是，那就足夠了。
簡直像在跳舞一樣輕快，像光一樣迅速，像閃電一樣激烈的一擊。
那就是杜蘭追求的美麗的力量其本身。
誰看了都不可忽視那狀態的真正的力量。
不管是浴血還是斬殺了什麼人，都是會讚賞的藝術。
被展現了那樣的東西的話，杜蘭已經沒有任何事能做了。

「隨你便吧」

杜蘭是想作為敗者獻出一切。
就算是被奪走生命也絲毫沒關係，他是那樣說的。
可是娜哈特只是露骨地皺眉了。
然後簡潔的一句。

「不，我又不需要大叔什麼的」
「什，我也才二十幾歳啊⋯⋯」
「欸⋯⋯⋯⋯完全看不出來⋯⋯」

杜蘭因為天生身體粗壯加上一直留著的邋遢鬍子，完完全全一點也看不出是二十多歳。

「看不出來⋯⋯」

追隨的艾夏好像也同意娜哈特。

「乾脆⋯⋯殺了我吧⋯⋯」

明明只是坦率地吐露了心裡的想法，不知為何愕然跪下的杜蘭卻感到了甚於剛才戰敗時的悲壯感。