奧茲瓦爾德和蕾蒂雪爾的視線靜靜地相互碰撞著。

「我知道了，我會借出力量的。」

看到接受了請求的蕾蒂雪爾，奧茲瓦爾德肩膀上的負擔一下子就消失了。即使不能讓她進入王家，也要把她留在這個国家。

「非常感謝，你應該需要一個研究機構吧，建築物、人員、資金什麼的我會提供的，你就在那專心研究怎麼樣？」

雖然是富有魅力的提議，但蕾蒂雪爾卻毫不留情地拒絶了。

「不，不必了。如果是研究術式的話，像以前一樣在學園裡就足夠了。有資金的支援就很感謝了，但是沒有必要大張旗鼓地準備專門的機構。」

轉生過來後，蕾蒂雪爾能做幾個自己想做的事情，其中之一就是盡情投入對魔術的研究。能一生研究魔術是最好的，不過，被關在設施裡進行持續研究與蕾蒂雪爾的性格相符。要搞研究的話，在哪裡都可以。說起來盧卡斯的研究室就夠充分了。

「嗯。」

對於奧茲瓦爾德，雖然想設立一個真正的專門機構逐一報告研究成果，但規模越小，機密性就越高。他也覺得這個條件也不壊。

「是嗎⋯⋯嗯，如果你這樣說的話，在學園內設立研究機構也不錯。有關研究的全權委託負責人就交給你了？」
「不，不用給我這麼重要的地位。」

面對奧茲瓦爾德的提案，蕾蒂雪爾閉上了眼睛，又說出了拒絶的話。

「我想集中精力搞研究，在學園內搞的話，是不是應該讓校長兼任呢？」
「⋯⋯那也是啊。我知道了，我會把這件事告訴盧卡斯的。」

就這樣，可喜可賀，盧卡斯的工作又增加了。

「感謝你為国家接受這個研究。」
「請不要在意。但是相對的，我也有兩點請求，陛下。」

蕾蒂雪爾用凜然的眼神凝視著奧茲瓦爾德。奧茲瓦爾德發現了那雙眼睛裡透出了無法形容的威嚴和性格，一瞬間確實被她壓制了。這個姑娘⋯⋯真是有趣。奧茲瓦爾德在心中暗暗地笑了。

「怎麼了，說說看。」
「我想請你保證我閱覽王国中書籍的權利。從借出東西到甚至能在禁書庫里睡覺，請允許我閱覽所有的資料。」

面對蕾蒂雪爾的要求，奧茲瓦爾德皺起了眉頭。

「閱讀書籍的許可，嗎？」
「是的，王城圖書館裡保管的資料⋯⋯公開和不公開全部都要。」

既然都到這個地步了，為了研究魔術就要創造一個舒適的環境。為此，首先需要資料，而且，剛剛聽到的那謎一樣的力量也很讓人在意。蕾蒂雪爾內心是這樣考慮的，不過蕾蒂雪爾所期望的事，大體上是奧茲瓦爾德沒預想到的事。

「想要那東西，你是打算幹什麼？」
「陛下，希望我能更好地研究的是陛下吧？然後我也希望能得到最好的結果。」

即使被人試探也那樣面無表情，蕾蒂雪爾只是稍稍地傾斜頭看著。

「我不是想要有關国家行政的機密文件。只是希望盡可能地需要研究所必要的知識。請問有什麼問題嗎？」

奧茲瓦爾德對蕾蒂雪爾的話保持沉默。確實有關国政的重要文件沒有保存在王城圖書館。所以即使接受了她的要求，現在也不會對奧茲瓦爾德產生不利吧。

「⋯⋯好吧。你就把国內所有的資料都用在研究上。」

這姑娘真的是從意想不到的地方提出了要求啊，但這也是她有意思的地方。

「非常感謝您。」
「這也是為了普拉提納王国。還有一個要求是什麼？」

因為奧茲瓦爾德問了，蕾蒂雪爾也就在這裡明說了。

「我想在郊外能準備一個房子。希望不要在這裡，而是在一個安靜、能沉浸在研究中的場所。」

對蕾蒂雪爾的話，到現在為止完全不能介入會話的宛如在蚊帳外的公爵家一抖地有了反應。蕾蒂雪爾瞥見了這些並忽視了，她深知公爵家討厭蕾蒂雪爾⋯多蘿瑟露，估計只會贊成不會反對的。所以沒有必要理睬他們，蕾蒂雪爾決定把他們當作不存在，只集中精力在王身上。

（廢棄了婚約後，能再離開公爵家就萬歳了）

在蕾蒂雪爾心裡想著這些的時候，奧茲瓦爾德對她要求稍微考慮了一下。對奧茲瓦爾德來說，他想在一定程度上對国家事業有關的魔術研究進行情報管制，也要隨時掌握研究進展。正因為如此，才想為她成立一個研究機構，但既然她拒絶了，就不能強行通過要求。

「嗯，那也是可以的。」

無論是給予研究機關還是給予別的房子，她都將被隔離，只要加強監視就好。奧茲瓦爾德抬起頭，看向公爵夫婦。

「我贊成她的意見，你們覺得這樣行嗎？」
「我們遵從陛下的意見。」

雖然完全沒有魄力，但只有速度是立即回答的，好像很希望她出去啊。哎呀，雖然是自己要出去的來著。作為想要趕走瘟神的菲利亞里吉斯公爵家，王的建議和禁忌之子的提案都能讓他們如願以償。公爵家看待連国王都低頭的女兒，比起諂媚更先感到恐怖。他們什麼都不知道，魔術的事，魔導術式的事。自己討厭和蔑視的女兒的價值，什麼都不知道。他們什麼都沒注意到，不管是從前還是今後。他們只想把蕾蒂雪爾的處置完全交給奧茲瓦爾德，都不知道自己放走了多大的獵物。
確認了菲利亞吉斯家的意見後，奧茲瓦爾德看向了蕾蒂雪爾。

「那麼就由我們這邊就準備房子吧。對了⋯⋯正好尼爾瓦恩郊外有個好房子，讓他們去安排那個地方吧。給你派遣傭人和廚師。」

面對奧茲瓦爾德的提議，蕾蒂雪爾慢慢地搖了頭。關於房屋的安排當然沒得說。

「非常感謝您的關心，但是我有想使用的侍從，就不用準備了。」

對於在前世，不管在哪裡被丟下也能活的家事通的蕾蒂雪爾，老實地說傭人之類並不需要。

「但是，你的日常生活怎麼辦？」
「關於這件事，希望您能聽我任性的要求，我想帶著現在公爵家工作的路維克和妮可兒這兩個傭人去，有他們就足夠了。」

雖然不需要傭人，但路維克和妮可兒則另當別論。不是作為傭人，而是作為從者或者說同居人那樣的存在的可以信賴的人放在身邊。

「但是⋯」

蕾蒂雪爾堅決地對皺著眉頭的奧茲瓦爾德說道。

「不如說，給我配置太多傭人的話反而會阻礙我的研究。」

在前世那個戰亂的時代，就算是王族也不能雇傭很多傭人。所以蕾蒂雪爾並不喜歡別人在自己家裡居住的感覺。家是舒心的場所，為什麼要被不認識的傭人圍著轉呢，就不能振作精神過日子嗎，這就是她這麼認為的。
對於自由自在並且能處理家務的蕾蒂雪爾的來說，大量的傭人不好意思地說只是礙事。
對於拒絶了傭人的蕾蒂雪爾，奧茲瓦爾德又看向了公爵。

「斯卡洛，多蘿瑟露小姐這樣說了，你沒意見嗎？」
「嗯，沒事。」

斯卡洛沒有猶豫。花了１６年，禁忌之子終於離開了這個家。為此，如果必要條件只是兩個傭人，他們很樂意奉上。

「房子的事情和研究機關的事都趕緊安排吧。」
「請拜託您了。」

蕾蒂雪爾和奧茲瓦爾德的對話，以雙方的利害一致的圓滿的形式迎來了結局。

「那麼，我該回到王城了。」
「需要我把您送到大門嗎？」
「不，不用那樣。」

所有的談判都結束了，奧茲瓦爾德和席利烏斯都回去了。他們的身影消失在接待室外後，蕾蒂雪爾鬆了一口氣。一瞥公爵家的眾人視線，他們就像是鬆了一口氣般的嘲笑，儘管如此，還是把所有的仇恨都投了進去。瞥了一下公爵家眾人的視線，他們就像是鬆了一口氣般的嘲笑，而且還帶入了所有的仇恨的情緒。
但是一想到這是最後一次見到這情形，反而覺得他們很可愛（？？？）。蕾蒂雪爾眯起眼睛微微一笑。大概是誤以為這是恥笑吧，斯卡洛顫抖著瞪著這邊，捏緊了拳頭，臉漲得通紅，然而蕾蒂雪爾卻不在意這些。
和王結束交涉後，蕾蒂雪爾腦內就把公爵家什麼的拋到天邊了，而且是更進一步的吹到了天邊的對邊。

（那麼，該回房間裡了）

將完全沒有興趣的斯卡洛等人的存在從記憶裡完全消去后，蕾蒂雪爾精神抖擻地走出了接待室。這樣一來，就能更加自由自在地從走廊裡走過去了。

「就是這樣，你們兩個人就要我一起搬到了新房子。」
「我不是很懂『就是這樣』是什麼意思。」

蕾蒂雪爾的閨房有些變化。回到了自己房間的蕾蒂雪爾把各種重要的話都刪去了，突然地說道。作為當事人的路維克和妮可兒對著突然說出這話的主人浮現了不同的表情。妮可兒一臉懵逼，路維克則是放棄了。

「與其說我是公爵家的，倒不如說我是侍奉小姐的。我不管到哪裡都會跟著你的，大小姐。」
「我，我也陪您去！因為我也是大小姐的專屬侍女！」

一邊吐槽一邊說一同去的路維克和，純粹仰慕著自己的妮可兒，蕾蒂雪爾對如此值得信賴的兩個傭人微笑著。

「謝謝你們兩個了。」
「但是如果說要轉移駐地的話，那換衣間不就要被收拾起來了？不能幫大小姐穿衣打扮什麼的，我不能忍。」
「嗯？等等，妮可兒，你嘆息的是那種事嗎？」

蕾蒂雪爾沒有自信能夠熟練地穿上在服裝房間裡的大量禮服和首飾。所以以這次搬家為契機，蕾蒂雪爾想把換衣間的東西收拾到一定程度。

「怎麼辦呢⋯⋯大小姐還沒有像寶石一樣地閃耀，不能展示那個美麗什麼的好過分⋯⋯！」
「⋯」

但是看到妮可兒那臉上浮現出了世界末日般的愁眉苦臉，思考了下後，還是決定不收拾就那樣全部拿走。

「真的嗎！？換衣間的東西，不處理掉嗎！」
「哎⋯⋯嘛，我哪邊都無所謂⋯⋯」
「非常感謝，大小姐！今後我也會竭盡全力向周圍的人傳遞大小姐的魅力！」
「⋯」

蕾蒂雪爾想著自己說不定是在自掘墳墓，但是妮可兒看起來很高興，所以這樣就行了，嗯。
幾輛馬車穿過王都尼爾瓦恩的大街。在潛伏用的簡樸的馬車中，奧茲瓦爾德挽著手閉著眼睛。他對面的席利烏斯面露難色。

「陛下，那樣做真的好嗎？」
「啊，沒有比這更好的結果了。總之，因為我和那位女孩都得到了想要的東西。」
「⋯」

雖然這麼說，席利烏斯還是皺著眉頭。

「但是⋯⋯是不是有點太過於讓步了？」
「我自信這對這個国家的發展是必要的。這麼些許的交易，不是什麼大不了的事。不如說和今後的事相比就是些細枝末節。」
「可能是這樣吧⋯」
「而且，我們沒有遭受不利因素。因為機密院保管著絶密文件，所以沒什麼問題。」
「⋯⋯一切照陛下的意願。」

然後在馬車裡安靜了一會兒，席利烏斯突然開口了。

「對了，陛下。第一王子殿下的事⋯⋯您打算怎麼做？」
「羅什福德在昨天的騷動中精神崩潰了。已經不是能繼承王位的狀態了⋯⋯」

由於前幾天的騷動的影響，第一王子羅什福德心理髮生了異常。瘋狂地鬧著叫著，莫名其妙的打轉，現在被安置在自己的房間監視下。

「連御醫都束手無策了⋯⋯不是一朝一夕能治好的吧。暫時先讓他離開王都，在某個安靜的地方進行療養比較好吧。」
「⋯⋯知道了，請交給我安排吧。那樣的話就需要把其他王子殿下叫回国家。」

王位繼承者不在自己的国家是奧茲瓦爾德的問題。普拉提納王国有三個王子，第二、第三王子都在国外。

「⋯⋯是啊。一回到王城就寫敕書。讓使者等著。」
「我知道了。」

載著普拉提納王国的国王和宰相的馬車靜靜地穿過維亞特里斯城的正門。