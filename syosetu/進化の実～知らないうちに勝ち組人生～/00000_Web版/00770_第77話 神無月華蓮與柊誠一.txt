﻿
我──神無月華蓮戀愛了。
小時候要強的我，去幫助其他被欺負的女孩子，和現在不同的是，那時候的我留著短髮看起來像個男孩子一樣，經常被同年齡的男孩子欺負。
我的家──【神無月集團】這名字在不熟識社會的孩子面前，是沒有任何意義的。嘛，就算是有意義，也不打算做些什麼。
在那之中，像平常那樣幫助被欺負的女孩子，在將矛頭指向我的時候────與他相遇了。

「不可以欺負女孩子！」

那個聲音的主人，是使我墜入愛河的──柊誠一。
雖然高中時的誠一君和小時候一樣，因為樣貌而被欺負。即便如此雙親還活在世上，沒有因此而志氣消沉。
然後，為什麼從小被欺負的誠一君會庇護著被欺負的我。
在那時，我與誠一君還不知道對方的名字。即便是遇到誠一君被欺負的情況下，作為女孩子同伴的我來說，也只是裝作沒看見毫不相關。
不出所料，欺負的矛頭指向了誠一君。

「感覺很噁心的別向我搭話啊，笨蛋。」
「哈啊！？說別人笨蛋的人才是笨蛋吧！」
「真煩啊，區區醜八怪。」
「哈啊！？說別人是醜八怪的──誒！？這是真的誒！？」

誠一君從以前開始就是這樣了。
與被欺負無關，讓人欽佩的態度。
但是，以失去雙親為開端，那種態度也隨波逐流了。
以前遇到誠一君的雙親，和想像中的差別很大，全力支撐著被欺負的誠一君，即便是身為局外人的我也能明白他被深愛著。
正因為被這樣的雙親養育長大，即便誠一君被欺負了，性格上也沒有扭曲的成長。
於是，欺負組的男孩子們，就開始對誠一君實施暴行了。

「去死吧，渣宰。」
「等！？好痛！反對暴力！」
「吵死了！」
「咕哈！？」

一個勁地不斷拳打腳踢，不一會兒誠一君就變成破抹布了。
即便我多麼要強，但因為男孩子們認真起來對誠一君的暴行而感到膽怯，沒能上去阻止他們。
那時，總算行動的我立刻跑到了誠一君面前。

「不、不要緊吧！？」
「嘿......看到煙花了啊......。」
「怎麼會！有、有誰來！叫下救護車......！」
「騙你的！騙你的啦！那是開玩笑的抱歉！」
「什！？為什麼要這麼做！？」

稍微以強硬的語氣詢問後，誠一君難為情地回答道。

「......因為、很害怕的吧？所以、為了讓你能夠安心......。」

儘管已經破破爛爛的了，誠一君也為了讓我能夠安心那樣開玩笑道。
因疼痛扭曲了表情，我不由得對爬起來的誠一君詢問道。

「為什麼要幫助我？你和我的初次見面吧？」
「初、初次見面？雖然我不太明白，你居然懂這麼難的語言呢。」
「認真地回答我。」

我這樣說後，誠一君理所當然般地說道。

「因為女孩子困擾著啊，不去幫助可不行。」
「哈？」

口中漏出了不知所云的聲音。
然後，誠一君繼續說道。

「父親說過，不愛護女孩子可不行！當然，我也是這麼想的。」

誠一君這樣說著笑了笑。
至今為止沒被當作女孩子對待的我，因為誠一君的那句話臉上開始發熱，不由地說出了動搖的話語。

「怎、怎麼會......像我這樣是個女孩子......。」
「嗯......我覺得很可愛的吶。」

進一步追擊那樣，誠一君說出了這句話。
因為這句話，臉上被染紅。

「你，你說......得還真是直白呢。」
「因為母親說過，不將想到的事情一清二楚地說出來可不行哦？雖然傷害對方的語言是不可以的，但如果是讚美的，覺得很厲害的，不用顧慮儘管說出來這樣！」

的確，像這樣面對面地對話是第一次，但作為被欺負的孩子的意義上而出名的誠一君，有著好幾次能夠在遠處看見他的機會。那個機會全都是誠一君被欺負的現場。他想要他們停下來，於是不由得開口了。
但這行為好像煽動了欺負組孩子們的嗜虐心了嗎，什麼也做不到。
其他還有，明明什麼事都沒做的誠一君卻在女孩子之間被討厭著，對女孩子說出像現在這樣率直的語言，被說成是充其量只會感到噁心罷了。
但是，對於我來說這是率直地將心情表達出來，對於陪同父親大人聽過各種各樣的大人的對話的我來說，純粹地感到高興。
比這些更重要的是，誠一君他那不會厭倦的笑臉十分耀眼。
在此之後，我與誠一君開始共同行動了。
那是想要從至今為止無視誠一君的罪惡感中逃跑的，自我滿足的行為。
但是，注意到後誠一君對於我來說，變成了不能失去的存在了。
從前就如風一般的翔太，和現在不同膽小鬼賢治，認識了誠一君的青梅竹馬們。
對於我來說，那是少有的同年代的朋友。
這全都多虧了誠一君。
可以說成是正因為和誠一君相遇，才有了現在的我。
在這幸福之中，然後發生了讓我喜歡上誠一君的某個事件。
那正好是世界不景氣的時候，神無月集團反而抓住了這個狀況，持續伸展自己的業績。
然後因為神無月集團的原因，捲入了被剝奪掉工作的人們的復仇裡面，於是我被誘拐了。
那時候，我以外還有誠一君以及翔太。被拐走的只有我和翔太兩人，而誠一君不知道為什麼被放置在那個地方了。
誘拐者要求我們的贖金。
為此鄭重地對待我們，並沒有對我們施加暴行。
即便如此，實際上不僅是被誘拐，還持有著槍，為此賢治經常哭了起來。稀奇的是，這時候翔太也害怕地哭了起來。嘛，考慮到那個年齡的話也是沒辦法的。
雖然我想著絕不能哭出來，但是內心感到非常地不安。
然後這個狀態持續了一段時間。

「大家家家家家家家！在哪裡啊啊啊啊啊啊啊啊！？」

這時────聽見了誠一君的聲音。
的確不知為何只有誠一君沒有被誘拐，但是不知道他是怎麼來到了這裡的。
然後誘拐犯們也是同樣驚訝，留下來看守的幾個人立刻朝著誠一君那邊跑去。

「喂......為什麼你知道這裡？」
「從華蓮的叔叔那裡聽到的！」
「哈？叔叔？」
「嗯！給叔叔打電話聽到的！」

聽到這些話，我立刻就理解了。
誘拐犯們要求贖金，然後父親告訴他了收取贖金的地方了吧。
恐怕在只有誠一君一個人被留下來之後，就立刻將這件事情告訴給了父親，然後偶然地在電話中的地點吧。
就算事實上就是這樣，但居然一個人前來了......。

「嘛算了......你來幹什麼？故意來被抓嗎？」
「不對！將大家還回來！」
「還以為你要說什麼......小少爺，我們可不能就這樣老老實實地放你們回去。」
「這種事情鬼才知道！好了快還回來！」

誠一君這樣說後，就被誘拐犯中的一人踹飛了。
但是，孩子不但敵不過大人，而且被簡單地踢飛。

「咕唔！？」
「小少爺......我們啊，因為這傢伙父母的錯，失去了工作妻子女兒。能明白嗎？小會社裡令人不悅的上司不斷剝奪我們的成果，在會社裡每天被當作無能來處理......但是，即便如此我們努力的汗水結成了結晶，如果能為別人起到作用的話，不站在表面的舞台上也可以。但是，神無月集團剝奪了我們的工作，不費吹灰之力地擊潰了會社，連養家的錢都沒有、然後妻子離開了......不僅如此，為什麼只有剝奪我們成果的那傢伙能夠在那神無月集團就職？那我們呢？我們的努力算什麼！？不知道艱苦的小屁孩別來妨礙我們！」

誘拐犯的頭領這樣說後，從周圍的誘拐犯那邊聽見了抽泣的聲音。
雖然想得太天真了，但是我同情他們。
如果他們所說的是真實的話，讓他們痛苦的不僅僅是神無月集團，還有那個會社裡的上司。但事到如今對於他們來說，這些事情怎樣都好。
這樣想的時候，被踹飛的誠一君忍受著疼痛站了起來，然後含著淚水大叫道。

「吵死了啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」
『！？』

沒想到身為受的誠一君會大叫道的我們，全員睜大了眼睛。

「我感謝生下我的母親！雖然感謝......但是看著我哦！？哪裡存在夢想！？將來是什麼！？這樣的胖子又臭的醜八怪！並不是我想胖才胖起來的......也不是我想身體臭才臭的......！但是，能夠明白因此而被欺負的我的感受嗎！？大叔們能夠結婚的吧！？也能夠工作的吧！？別說那麼奢侈的話啊！我每天都在拚命地活著啊！？也很害怕去學校！害怕和母親以外的人扯上關係！活著感到非常地幸苦！即便如此，比我更加幸苦的人還有許多許多！所以我才努力地活著哦！？如果我死了的話，父親和母親會很傷心的！不管多麼辛苦，我都會努力地活著！和這比起來，大叔們是受到恩惠了的吧！？為什麼就不明白這一點！？」
「但、但是......小少爺還是個孩子────。」
「是孩子又怎麼了！？你看！覺得這樣能一下子就變帥嗎！？有著光明的將來嗎！？」
「............。」
「雖然明白但是無言是很殘忍的啊！」

誘拐犯們僅僅只是被壓倒著。
從誠一君口裡所說出來的語言份量，對於他們來說是非常沉重的。
的確，他們的現狀或許非常地幸苦。
但是，和誠一君比起來的話，正面地與數年份在這之上的幸苦戰鬥著。
誘拐犯們完全同情起誠一君了。

「小少爺......你也真是幸苦啊......。」
「打起精神吧......人生，是不知道將會發生什麼事的呢......吶？」
「那個、給你糖吃啦......。」

不如說是全力地安慰他。
誠一君絕不是不勤奮，運動除外。
然後，比起這些還要殘酷的是，在這個世界上第一印象幾乎都是由容貌所決定的。
誠一君的容貌，以世間標準來看是非常醜陋的。對於我來說，那是可愛之處。
誠一君要獲得一般的幸福的話，那是不得不轉生才行。
因此，和誠一君所說的一樣。不管是誰來看，誠一君的未來都是一片黑暗。

「小少爺......我做了很不得了的事情......即便如此，也不要緊嗎......？」
「不要緊的。因為，沒有有給任何人留下痛苦的回憶。況且，讓你想起不好的事情的話，不道歉可不行呢。」
「......是嗎......是這樣啊。」

於是，我們平安無事地被解放，誘拐犯們也到警察局自首去了。
我們很快和誠一君合流後，誠一君非常地擔心我們。
翔太他們是因為哭淚了嗎，現在睡著了。

「大家不要緊吧！？沒有受傷吧！？」
「不要緊哦。說起來誠一君你......非常地亂來呢。」

不由得苦笑道後，誠一君注視著我的臉龐。
然後────。

「抱歉，很害怕吧。但是......不要緊了。」
「啊────。」
「真了不起。」

誠一君輕輕地把我抱在懷裡，然後撫摸著我的頭。
被巨大的身體所包圍，我被可靠的安心感所裹住。
然後，至今為止所忍耐的一口氣溢了出來，最終我哭了出來。

「嗚......嗚......嗚哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇！好、好害怕啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」
「嗯、恩。」
「還、還以為會被怎樣......抽泣......對、對待......。」
「不要緊，不要緊哦。」

誠一君在我停止哭泣前都再溫柔地撫摸著我。
將臉埋在他的胸口後，他的氣味就胸中擴散開了。

「吸......是誠一君的氣味......。」
「誒！？抱歉，很臭吧！？抱歉抱住你了！？」

用力抱住打算馬上拉開距離的誠一君，我對他的氣味心滿意足。

「完全不......我很喜歡、你的氣味。」
「......華蓮......你的鼻子沒問題吧？」
「真是失禮啊。這可是自己承認自己很臭了哦？」
「嗯、這樣說著我都想哭了。」

這樣坦然自若地對話，我不由得露出了笑容。
或許周圍的人把誠一君說的很過分也說不定。
即便如此，對於我來說，誠一君暴露出自己的本性、在一起很開心、能夠安心的存在。
這件事情，經由了這次偶然事件後我更加有了實感──我喜歡他。
想要一直呆在他身旁，一起歡笑。
是個讓我這麼想的一個人。
────在那之後，沒有將誘拐犯們抓捕起來。
要問為什麼的話，聽取緣由的我的父親，徹底地調查了他們原上司，確認真偽。確認他們所說的是事實後，將他們領了回來。
因為這件事情，發現像這次這樣被手下們討厭的上司，誘拐犯的原上司立刻解僱，誘拐犯們在神無月集團活躍著。
向他們詢問當時所發生的事情後，他們都露出笑容回答著同一句話。

『被比起我們更加幸苦的小少爺訓斥了，才有今天的我們』

誠一君與他們的相遇，結果上讓他們露出了笑容。
所以，我也想讓誠一君露出笑容。
在我喜歡上誠一君的同時，就下定了這樣的決心。

