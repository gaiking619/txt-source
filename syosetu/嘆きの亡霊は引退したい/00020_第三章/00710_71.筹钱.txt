離開寶具店，對快步前往氏族建築的我，緹諾低聲問道。

「Master......那個......錢不是不夠嗎？」

「好不容易遇上了......可不能錯過那件寶具」

我確實欠著債。但如果放掉這個機會，『轉換的人面』再也不會出現在眼前了吧。

持有最適用於犯罪的『轉換的人面』之所以不違法，是因為若把持有這類寶具定為違法，就會妨礙獵人從寶物殿帶回寶具。

澤布魯迪亞是憑借尋寶獵人的力量發展起來的國家。在寶物殿發現的寶具，不是鑑定師的話就無法判斷其能力。如果發現拚命帶回的是違法寶具而被逮捕，獵人們也就不會把寶具帶回帝都了。

不僅是『轉換的人面』，帝國法律甚至允許持有所有的寶具。

但反過來說——只是持有無罪而已，若問會不會把違法性高的寶具擺在店內，回答是NO。只要思考正常，我不認為誰會去賣使用有罪的寶具。

把不擺在店內的寶具拿到手的手段很有限。更不用說，甚至有可能在我有生之年都不會再出現。
這是一個千載難逢的機會。即使當掉父母也應該入手。

「......緹諾啊......那個......呢,你有多少存款？」

「！？」

「不是，這是參考。只是作為參考，而已哦。我會向西特莉借所以沒事的。嗯，沒事的」

然而最近才討論過債務的事。時機太過不好，胃難受起來。

「Master......那，那個地步——」

緹諾退縮了一下，一臉愕然地嘟囔著『那個寶具是，能讓Master說到那個地步的東西嗎？和以前，持有的噁心面具有點像......』之類的。如果我說，這是為了能一個人逛甜品店這種話，她會露出什麼樣的表情呢。

沒問題。向西特莉借的話沒問題。就算是再便利的違法寶具，我也不認為會變得那麼貴。
西特莉是鍊金術師。正如其名，可造出金錢！雖然不知道是做什麼來賺錢的，但是她能砰地一下掏出上億的金錢，所以沒問題！

總覺得遲早要成為克萊伊·斯馬特，我好害怕。

氏族建築前面停著大型馬車。
是一輛顯得很費錢的黑色拋光馬車。拉車的是兩匹體格健壯的黑馬，仿彿威懾似地把視線投向四周。
車身上刻著模仿三把劍相交的紋章。

緹諾看到紋章，露出詫異的神情。

「格拉迪斯的家紋......？那個家系應該是討厭獵人的......」

格拉迪斯伯爵。連不諳世事的我也認識。所屬於澤布魯迪亞，是擁有實力的貴族之一。
被稱為澤布魯迪亞的『劍』，是一個長年守護帝國的武家。
擁有的領地雖然沒有多廣闊，但領地內卻存在很多寶物殿，並且會定期送入麾下的騎士團，因此擁有許多令獵人甘拜下風的強大士兵。在寶物殿眾多的澤布魯迪亞，騎士團的每一個人都，吸收了或多或少的瑪那源，但似乎還是有別於格拉迪斯的騎士團。

因為我們，《嘆息的亡靈》一直注意不與貴族有過多瓜葛，所以知之不詳，不過正如緹諾所說是個討厭獵人的家系，也能說是這類貴族的代名詞。
雖然以前在某個聚會上與家主見過面，但我記得被他用可怕的眼神射殺似地瞪了。

每個路過的人，都一臉疑惑地看著馬車，或許是因為知道那個家系把獵人視為眼中釘吧。

又是麻煩事嗎？討厭獵人的家系的馬車為什麼偏偏停在了《足跡》的氏族建築前面啊？
應該不是來找麻煩。但是，就算要來委託什麼，先通過探險者協會才合乎情理吧。

緹諾一動不動地仰視著我，等著我開口。

明明現在很忙，真是麻煩。下跪磕頭的話會回去嗎。
正想著那種失禮的事，馬車門突然打開了。

「謝謝你的送行。艾克蕾爾小姐。請代我向格拉迪斯卿問好」

「嗚姆。無須在意。雖然不太喜歡獵人，但武人不同。亞克，期待哪天還能領教你的劍術」

「啊——」

出現的是，亞克和身穿白色禮服的女孩。應該是為了警備吧，四周圍著幾個裝備了擦得鋥亮的輕鎧的騎士。
我趕緊收住對盼望已久的身影不禁發出的聲音。因為一同出現的女孩顯然是貴族階級。

雖然我和青梅竹馬不同，不會性急易怒到頂撞貴族，但因為沒有學問且教養不太好，所以有貴族在的場合我會盡量保持沉默。

少女抬頭凝視著突然喊過去的我。雖然毫無斑點的白色肌膚與清澈通透的藍色眼眸擔保了少女將來能長得美麗動人，不過她的目光卻有種看低我的盛氣凌人感。
年齡是否到十歲了呢。儘管我們那時都熱衷於成為獵人，但這個年紀就有如此銳利的眼神，所謂的貴族教育想必很嚴格吧。

純白的禮服似乎是便裝，穿著自然得體的那副身姿，確實擁有立於人上者特有的威容。
幾乎沒有裝飾的禮服和，腰間佩戴的裝飾過多的短劍，表明了那個少女的來歷。

少女用尖銳的聲音命令道。

「你這傢伙，是什麼人？礙事，從那滾開」

「對M，Master什麼口氣——姆嘰！」 

把走到前面反射性地要咬上去的緹諾從後面抱住，用右手捂住她的嘴。居然想扯上明顯的麻煩事，你是莉茲嗎？

對露出溫和的笑臉正要讓路的我，亞克浮現爽朗的笑容並說出多餘的話。

「啊，克萊伊。我剛回來。承蒙格拉迪斯卿的好意，派了馬車送我......她是卿的女兒，艾克蕾爾小姐」

又是新后宮嗎？不管怎樣這個年齡也不行不是嗎。沒想到亞克竟然有蘿莉控的傾向。
全力忍住將要說出的俏皮話。不行，還不能說。如果在這位千金小姐面前說出那種話，我的頭會被砍掉的。

雖然我和亞克關係比較好，會互相開玩笑，但和這位千金小姐才剛見了面。

可是，邀請了亞克的應該是桑多萊茵侯爵——對獵人也是穩健派的人。為何竟然會把格拉迪斯的女兒給帶來啊？......雖然比把家主帶來更好。

艾克蕾爾小姐聽到亞克的話，瞪大了眼睛。像怒視一樣把我從頭看到到腳。

「............你這傢伙就是，那個《千變萬化》嗎。常聽我父親談到」

措辭和態度雖然傲慢，但聲音卻是小孩子特有的尖銳聲。
即便是我也不會因小孩子的話而膽怯。

艾克蕾爾小姐用爽快的語調繼續說道。看守四周的騎士們的表情緊繃著。

「正如父親所說，是個意外地看起來弱小的男人。你這傢伙竟是超過亞克·羅丹的獵人，真是難以置信」

「............」

「探險者協會也墮落了呢。難道是用金錢買的地位？你這骯髒的獵人，最好感到羞恥」

「............」

「............被我說到這種地步，為何一聲不吭？你這傢伙沒有自尊心嗎？」

大小姐不知為何後退了一步，以一副仿彿看到了可怕的東西似的表情向我問道。

我恢復一直憋著的呼吸，盡可能用不顯得無禮的溫和聲音說道。
懷裡的緹諾扭動著身體，但我無視了她。

「我的教養很差，也不懂禮儀，所以決定盡量少說話」

「ッ！？嗯............呃..................咳咳！嗚，嗚姆。不，不錯的用心」

艾克蕾爾小姐像受到打擊一樣左右張望，輕輕乾咳了一聲說道。

就算惹貴族不高興也沒有任何好處。我又不用依靠權威來圖得利益，也正如這位少女所說沒有自尊心，因此下跪磕頭能解決的話我會下跪磕頭。......下跪磕頭的話會借錢給我嗎。

在馬車門處待命的風度翩翩的老人，小聲地對艾克蕾爾小姐說道。
是個全身上下穿著黑色管家服的男人。恐怕，是她的看護者吧。

「小姐，時間差不多到了」

「唔，嗯！」

像是被那句話救了一樣，艾克蕾爾小姐氣勢滿滿地抬頭看向亞克。

「那麼，亞克。以後再見啦。到訪格拉迪斯領時，聯繫我的本家就行。下次再陪我練劍吧！」

居然要陪貴族千金練劍，看來亞克也很辛苦。

說完想說的話，艾克蕾爾最後狠狠地瞪了我一眼，便帶著隨從乘馬車離去了。
真是個暴風雨般的千金小姐。長大後會變得和莉茲一樣嗎......會不會呢。

總算可以呼吸了。好奇地窺視著這邊的圍觀人群四散而去。
亞克嬉皮笑臉地走了過來，態度和藹地道了歉。

「不好意思，沒進行聯絡就突然坐著馬車來了。說是無論如何都要送我，實在無法拒絕」

亞克。是亞克。用卡牌來說就是王牌。這下子就算發生麻煩事，也能全部解決了。可以的話希望能盡量留在氏族建築。

「來的正好。亞克啊，能借錢給我嗎？」

「誒？」

引以為榮的《起始的足跡》最強的男人，一臉痴呆地看著我。
